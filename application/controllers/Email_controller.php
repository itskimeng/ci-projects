<?php 

class Email_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('form');
    }

    public function index() {

        $this->load->helper('form');
        $this->load->view('email_form');
    }

    public function send_mail() {
        
        
		$to = 'markkimsacluti10101996@gmail.com';
        $subject = "Safety Seal Email Verification";
        $message = " Welcome to DILG CALABARZON Safety Seal Portal! <br> To complete your Safety Seal profile, we need you to confirm your email address. <br><a class='btn btn-light-primary' href='http://safetyseal.calabarzon.dilg.gov.ph/application/functions/verify.php?vkey='>Verify Account</a>";
        $headers = "From: safetyseal@calabarzon.dilg.gov.ph \r\n";
        $headers .= "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-800" . "\r\n";
		$retVal = mail($to, $subject, $message, $headers);
				if($retVal == true ) {
					echo "Message sent successfully...";
				 }else {
					echo "Message could not be sent...";
				 }
    }

}

?>
